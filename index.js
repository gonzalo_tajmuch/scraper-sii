const ScraperSii = require('./lib/scraper-sii');

/** @param {object} opciones launchOptions para puppeteer.launch */
module.exports = (opciones) => {
    const scraper = new ScraperSii(opciones);

    return {
        autentificarse: async (rutEmpresa, clave) => await scraper.ingresarAlSii(rutEmpresa, clave),
        cerrar: async _ => await scraper.browser.close(),
        get navegador() { return scraper.browser },
        get pagina(){ return scraper.page }
    }
};