const puppeteer = require('puppeteer');

class ScraperSii {
    /** @param {object} options launchOptions para puppeteer.launch */
    constructor(options = {}) {
        if (options && typeof options != typeof {}) {
            throw 'Debe entregar un objeto como parámetro "opciones" de scraperSii.' +
                'Si desea correr el navegador con cabeceras envíe el objeto opciones con ' +
                'el atributo headless = false.'
        }
        this.options = options;
        this.reintentos = 0;
    }

    async ingresarAlSii(rut, clave) {
        try {
            if (this.browser) {
                await this.browser.close();
            }

            await this._autentificarseConElSii(rut, clave);
            this.reintentos = 0;

        } catch(exception) {
            console.log(`scraper-sii: ${exception}`);
            
            if (this.browser) {
                await this.browser.close();
            }

            if (exception.name == "TimeoutError") {
                console.log(`scraper-sii: Reintentando...`);

                if (this.reintentos < 5) {
                    this.reintentos++;
                    await ingresarAlSii(rut, clave);

                } else {
                    throw exception;
                }
            } else {
                throw exception
            }
        }
    }

    async _inicializarNavegador(url) {
        try {
            console.log('scraper-sii: inicializando navegador en ' + url);

            const noSandbox = {args: ['--no-sandbox']}; // default;

            const launchOptions = { ...noSandbox, ...this.options } // this.options predominante

            this.browser = await puppeteer.launch(launchOptions);

            this.page = await this.browser.newPage();
    
            await Promise.all([
                this.page.waitForNavigation(),
                this.page.goto(url)
            ]);
            
            console.log('scraper-sii: navegador inicializado');

        } catch (exception) {
            throw exception;
        }
    }

    async _autentificarseConElSii(paramRut, paramClave) {
        try {
            await this._inicializarNavegador('https://zeus.sii.cl/AUT2000/InicioAutenticacion/IngresoRutClave.html');

            console.log('scraper-sii: autentificandose con el SII');
            // TODO: validar página válida para la acción
            await this.page.evaluate((rut, clave) => {
                document.querySelector('input#rutcntr').value = rut;
                document.querySelector('input#clave').value = clave;
            }, paramRut, paramClave);
            
            try {
                await Promise.all([
                    this.page.waitForNavigation(),
                    this.page.click('[title=Ingresar]')
                ]);
            
            } catch (error) {
                const mensajeAlerta = await this.page.evaluate(() => {
                    const alerta = document.querySelector('#alert_placeholder span');
    
                    if (!alerta) return '';
    
                    return alerta.innerText;
                });

                console.log('scraper-sii: credenciales inválidas');
                console.log('scraper-sii: cerrando navegador');
                
                if (this.browser) {
                    await this.browser.close()
                }

                throw {
                    name: 'BadCredentialsError',
                    message: mensajeAlerta,
                }
            }

            try {
                await this.page.waitForNavigation()

            } catch (error) {
                const mensajeError = await this.page.evaluate(() => {
                    // primer párrafo en el elemento.
                    const elemTitulo = document.querySelector('#titulo');

                    if (!elemTitulo) return '';
                    
                    return elemTitulo.innerText.split('\n')[0];
                });
                
                throw {
                    name: 'BadCredentialsError',
                    message: mensajeError
                }
            }
                
            console.log('scraper-sii: autentificado');
        
        } catch (exception) {
            throw exception;
        }
    }
}

module.exports = ScraperSii;